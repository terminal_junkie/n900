# Custom i3wm For the Nokia N900 With PostmarketOS
The original wiki can be found [here](https://wiki.postmarketos.org/wiki/Nokia_N900_(nokia-n900))

Make sure the testing mirror is in /etc/apk/repositories and i3-gaps is installed

```bash
http://mirror.postmarketos.org/postmarketos/master
http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing
```

```bash
sudo apk add i3wm-gaps
```
The prebuilt .img should have all the rest to get started, download latest release [here](https://images.postmarketos.org/bpo/v21.06/nokia-n900/i3wm/)

Some packages I use and installed
```bash
acpi
catt
git
gotop
htop
imagemagick
nano
ncdu
neofetch
netsurf
nmap
oh-my-zsh
pfetch
py3-pip
python3-dev
ranger
thunar
tshark
wal
wavemon
wget
youtube-dl
zsh
``` 
